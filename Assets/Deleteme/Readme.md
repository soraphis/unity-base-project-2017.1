# Zweck dieses Ordners

Dieser Ordner enth�llt Anleitungen/Richtlinen und Beispiele die im Fertigen Produkt nicht mehr ben�tigt werden. Das Produkt muss funktionieren k�nnen, wenn dieser Ordner gel�scht wird.

_____


# Inhalt

| Datei/Ordner | Beschreibung                                             |
|--------------|----------------------------------------------------------|
| cube         | Ein 1x1x1 Cube, dient den 3D Designern als Grundlage zur Skalierung ihrer Modelle |